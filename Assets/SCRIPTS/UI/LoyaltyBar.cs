﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoyaltyBar : RadialBar
{

    public GameObject LoyaltyInfo;
    public Text Name, Progress;
    public PlayerController Player;
    public BaseWeapon Weapon;
    public float ActivationDistance;
    bool Showing = false;


    public void Update()
    {
        if (Vector3.Distance(Input.mousePosition, transform.position) < ActivationDistance)
        {
            if (Player != null && !Showing)
            {
                LoyaltyInfo.SetActive(true);
                Name.text = "Loyalty to " + Player.gameObject.name;
                Progress.text = (int)(Weapon.GetLoyalty(Player) * 100f) + "%";
                Showing = true;
            }
        }

        else
        {

            LoyaltyInfo.SetActive(false);
            Showing = false;
        }
    }


}
