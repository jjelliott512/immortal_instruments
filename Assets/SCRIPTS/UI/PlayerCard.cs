﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
public class PlayerCard : MonoBehaviour {

    public Text PlayerName;
    ImmortalNetworkManager Manager;
    NetworkConnection Connection;
    public GameObject ReadyButton;


    void Start()
    {
       transform.SetParent(FindObjectOfType<VerticalLayoutGroup>().transform);
    }
    public void Init(string name , ImmortalNetworkManager manager, NetworkConnection connection)
    {
        PlayerName.text = name;
        Manager = manager;
        Connection = connection;
    }

    public void SetReady()
    {
        ReadyButton.SetActive(false);
        GetComponent<NetworkLobbyPlayer>().SendReadyToBeginMessage();
    }

   
}
