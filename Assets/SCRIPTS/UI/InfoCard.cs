﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InfoCard : MonoBehaviour {

    public BaseWeapon Weapon;
    public Transform ChallengeTransform , LoyaltyTransform;
    public Text Name;
    public RadialBar MaliceBar;
    public GameObject RadialBarPrefab , ChallengeBarPrefab, LoyaltyBarPrefab;
    public LineRenderer LineRenderer;

    public void Populate(BaseWeapon weapon) {

        for(int i = 0; i<ChallengeTransform.childCount; i++)
        {
            Destroy(ChallengeTransform.GetChild(i).gameObject);
        }
        for (int i = 0; i < LoyaltyTransform.childCount; i++)
        {
            Destroy(LoyaltyTransform.GetChild(i).gameObject);
        }
        Weapon = weapon;
        Name.text = Weapon.gameObject.name;
        foreach(var m in Weapon.Modifiers)
        {
            var obj = Instantiate(RadialBarPrefab, ChallengeTransform);
            var bar = obj.GetComponent<RadialBar>();
            bar.SetMaxValue(1);
            bar.SetValue(1);
        }
        for (int i =0; i < Weapon.ActiveChallenges.Count; i++)
        {
            var obj = Instantiate(ChallengeBarPrefab, ChallengeTransform);
            var bar = obj.GetComponent<ChallengeBar>();
            bar.MyChallenge = Weapon.ActiveChallenges[i];
            bar.SetMaxValue(Weapon.ActiveChallenges[i].NeededCount);
            bar.SetValue(Weapon.ActiveChallenges[i].CurrentCount);
        }
        foreach(var loyalty in Weapon.Loyalties)
        {
            var obj = Instantiate(LoyaltyBarPrefab, LoyaltyTransform);
            var bar = obj.GetComponent<LoyaltyBar>();
            bar.Player = loyalty.Key;
            bar.Weapon = Weapon;
            bar.SetMaxValue(BaseWeapon.MaxLoyalty);
            bar.SetValue(BaseWeapon.MaxLoyalty * loyalty.Value);
        }
        MaliceBar.SetMaxValue(BaseWeapon.MaxMalice);
        MaliceBar.SetValue(weapon.Malice);
    }

    public void Clear()
    {
        Weapon = null;
        for (int i = 0; i < ChallengeTransform.childCount; i++)
        {
            Destroy(ChallengeTransform.GetChild(i).gameObject);
        }
        for (int i = 0; i < LoyaltyTransform.childCount; i++)
        {
            Destroy(LoyaltyTransform.GetChild(i).gameObject);
        }
        Name.text = null;
        MaliceBar.SetValue(0);
    }

    void LateUpdate()
    {
        if (Weapon)
        {
            Name.text = Weapon.gameObject.name;
            foreach (var m in GetComponentsInChildren<ChallengeBar>())
            {
                if (!m.MyChallenge) continue;
                m.SetMaxValue(m.MyChallenge.NeededCount);
                m.SetValue(m.MyChallenge.CurrentCount);
            }
            foreach (var l in GetComponentsInChildren<LoyaltyBar>())
            {
                
                l.SetMaxValue(BaseWeapon.MaxMalice);
                l.SetValue(l.Weapon.GetLoyalty(l.Player) * BaseWeapon.MaxLoyalty);
            }
            MaliceBar.SetMaxValue(BaseWeapon.MaxMalice);
            MaliceBar.SetValue(Weapon.Malice);
        }
    }
}
