﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
public class DamageNumber : NetworkBehaviour {

    public float DriftSpeed , Lifetime;
    public Color C1, C2;
    [ClientRpc]
    public void Rpc_Init(float amt)
    {
        GetComponentInChildren<Text>().text = amt.ToString("#.#");
        GetComponentInChildren<Text>().color = Color.Lerp(C1, C2, amt / 100);
    }
    public void Update()
    {
        Lifetime -= Time.deltaTime;
        if (Lifetime < 0) Destroy(gameObject);
        transform.position += ((Vector3)Random.insideUnitCircle + new Vector3(0,0.6f,0))* DriftSpeed;
    } 
}
