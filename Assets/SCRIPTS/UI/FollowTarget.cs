using System;
using UnityEngine;
using UnityEngine.Networking;

namespace UnityStandardAssets.Utility
{
    public class FollowTarget : MonoBehaviour
    {
        public Transform target;
        public Vector3 offset = new Vector3(0f, 7.5f, 0f);
        public float DeadZone;
        public float StopZone;
        public float Speed;
        bool IsMoving;

        private void Start()
        {
            if (!transform.parent.GetComponent<NetworkIdentity>().isLocalPlayer) Destroy(gameObject);
            transform.SetParent(null , true);
        }

        private void Update()
        {
            if (!IsMoving && Vector3.Distance(target.position, transform.position) > DeadZone)
            {
                IsMoving = true;
            }
            if (IsMoving && target)
            {
                if (Vector3.Distance(target.position, transform.position) > StopZone)
                {
                    transform.position = Vector3.MoveTowards(transform.position, target.position + offset, Speed);
                }
                else
                {
                    IsMoving = false;
                }
                
            } 
        }
    }
}
