﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class EndGamePanel : MonoBehaviour {

    public Text Name, WeaponName;

    public void Show(PlayerController winner)
    {
        Name.text = winner.name;
        WeaponName.text = winner.Weapon.name;
        gameObject.SetActive(true);
    }


}
