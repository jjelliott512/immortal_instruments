﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RadialBar : MonoBehaviour {

    float MaxValue, CurrentValue;
    public Image Target;

    public void SetMaxValue(float value)
    {
        MaxValue = value;
        DrawBar();
    }
    public void SetValue(float value)
    {
        CurrentValue = value;
        DrawBar();
    }

    public void DrawBar()
    {
        Target.fillAmount = CurrentValue / MaxValue;
        if (CurrentValue == MaxValue) Target.color = Color.green;
    }
}
