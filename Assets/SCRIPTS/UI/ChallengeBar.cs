﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChallengeBar : RadialBar {

    public GameObject ChallengeInfo;
    public Text RewardName, Info, Progress;
    public Challenge MyChallenge;
    public float ActivationDistance;
    bool Showing = false;


    public void Update()
    {
        if (Vector3.Distance(Input.mousePosition, transform.position) < ActivationDistance){
            if (MyChallenge != null && !Showing)
            {
                ChallengeInfo.SetActive(true);
                RewardName.text = MyChallenge.RewardName;
                Info.text = MyChallenge.ChallengeInfo;
                Progress.text = MyChallenge.CurrentCount + " | " + MyChallenge.NeededCount;
                Showing = true;
            }
        }
        
        else
        {
            
            ChallengeInfo.SetActive(false);
            Showing = false;
        }
    }


}
