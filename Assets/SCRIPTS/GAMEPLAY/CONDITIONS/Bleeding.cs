﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
public class Bleeding : Condition
{
    float Damage;
    BaseWeapon Owner;
    public float Lifetime = 2;
    public const float TickTime = 0.5f;
    float LastTick;

    public Bleeding(PlayerController target , float damage , BaseWeapon owner) : base (target)
    {
        Damage = damage;
        Owner = owner;
        LastTick = Time.time;
    }

    public override void Tick()
    {
        if(Time.time - LastTick > TickTime)
        {
            Target.Damage(Damage, Owner.User.GetComponent<NetworkIdentity>().netId);
            Lifetime -= Time.time - LastTick;
            LastTick = Time.time;
        }

        if (Lifetime < 0) Target.ConditionsToRemove.Add(this);
        
    }
}
