﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Condition {

    protected PlayerController Target;
    public Condition(PlayerController target) {
        Target = target;
    }
    public abstract void Tick();

}
