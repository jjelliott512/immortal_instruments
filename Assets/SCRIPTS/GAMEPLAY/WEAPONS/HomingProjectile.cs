﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class HomingProjectile : Projectile {

    GameObject Target;
    Vector3 VectorToTarget { get { if (!Target) return transform.forward; else return Target.transform.position-transform.position; } }
    public float LaunchForce;

    [Server]
    public override void Fire(Vector3 dir, PlayerController owner, ProjectileWeapon source)

    {
        base.Fire(dir, owner, source);
        GetComponent<Rigidbody2D>().AddForce(dir * LaunchForce);
    }

    [ServerCallback]
    protected override void FixedUpdate()
    {
        foreach (var p in FindObjectsOfType<PlayerController>())
        {
            if (Vector3.Distance(p.transform.position, transform.position) < VectorToTarget.magnitude && p != Owner)
            {
                Target = p.gameObject;
            }
        }
        Direction = VectorToTarget.normalized;
        transform.up = GetComponent<Rigidbody2D>().velocity.normalized;
        base.FixedUpdate();
    }
}
