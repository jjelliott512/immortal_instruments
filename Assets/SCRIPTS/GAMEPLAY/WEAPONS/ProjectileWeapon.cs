﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
public class ProjectileWeapon : BaseWeapon
{
    public GameObject ProjectilePrefab;
    public float Offset;
    public float SpreadAngle;
    public int NumProjectiles;

    public override void PrimaryAttack()
    {
        var dir = User.transform.up;
        Cmd_SpawnProjectile(dir);

    }

    public override void SecondaryAttack()
    {
        for(int i = -NumProjectiles; i <= NumProjectiles; i++)
        {

            var dir = Quaternion.AngleAxis((SpreadAngle / 5) * i, User.transform.forward) * User.transform.up;
            Cmd_SpawnProjectile(dir);
        }
    }

    [Command]
    void Cmd_SpawnProjectile(Vector3 dir)
    {
        var proj = Instantiate(ProjectilePrefab, transform.position + dir * Offset, Quaternion.FromToRotation(Vector3.up , dir));
        proj.GetComponent<Projectile>().Fire(dir, User.GetComponent<PlayerController>(), this);
        NetworkServer.Spawn(proj);
        
        
    }
}
