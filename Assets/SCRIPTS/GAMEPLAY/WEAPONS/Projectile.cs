﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

[RequireComponent(typeof(Collider2D))]
public class Projectile : NetworkBehaviour {


    public float Lifetime;
    public float Speed;
    protected float Damage;
    public int Penetration;
    protected PlayerController Owner;
    ProjectileWeapon Source;
    protected bool Fired;
    float StartTime;
    protected Vector3 Direction;
    protected Rigidbody2D Rigid;
	// Use this for initialization
	protected virtual void Start () {
        Rigid = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
    [ServerCallback]
    protected virtual void FixedUpdate () {
        if (Fired)
        {
            Rigid.AddForce(Direction * Speed);
        }
        if (Time.time - StartTime > Lifetime) Destroy(gameObject);
	}


    [Server]
    public virtual void Fire(Vector3 dir , PlayerController owner , ProjectileWeapon source)

    {
        Owner = owner;
        Direction = dir;
        Fired = true;
        StartTime = Time.time;
        Source = source;
    }

    [ServerCallback]
    protected void OnTriggerEnter2D(Collider2D col)
    {
        var combat = col.gameObject.GetComponent<PlayerController>();
        if (combat != null)
        {
            combat.Damage(Source.TrueDamage , Owner.GetComponent<NetworkIdentity>().netId);
            Penetration--;
            if (Penetration <= 0) Destroy(gameObject);
            //Play an effect!
        }
    }
}
