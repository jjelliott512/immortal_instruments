﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class BaseWeapon : NetworkBehaviour {

    public bool Equiped;
    public virtual void PrimaryAttack() { }
    public virtual void SecondaryAttack() { }
    protected GameObject LocalPlayer;
    public GameObject Info;
    public float ShowInfoDistance , LoyaltyGain , LoyaltyDecay;
    public GameObject User;
    public float TrueDamage { get { return BaseDamage + BaseDamage * Malice / MaxMalice + BaseDamage * GetLoyalty(User.GetComponent<PlayerController>()); } }
    public float BaseDamage;
    public float PrimaryCost , SecondaryCost;
    [SerializeField]
    public List<Challenge> ActiveChallenges;
    [SyncVar]
    public float Malice;
    [SyncVar(hook ="OnImmortal")]
    public bool Immortal = false;
    public Dictionary<PlayerController, float> Loyalties;
    public List<Modifier.Mods> Modifiers;
    public const float MaxLoyalty = 100f , MaxMalice = 100f , MaliceForKill = 10f;
    public ParticleSystem FlameEffect;

    protected virtual void Start()
    {
        ActiveChallenges = new List<Challenge>();
        Modifiers = new List<Modifier.Mods>();
        Loyalties = new Dictionary<PlayerController, float>();
        foreach (var p in FindObjectsOfType<PlayerController>()) Loyalties.Add(p, 0);
        foreach (var c in GetComponentsInChildren<Challenge>()) ActiveChallenges.Add(c);
        foreach (Challenge c in ActiveChallenges) c.Owner = this;
    }
    public void Equip(PlayerController user)
    {
        User = user.gameObject;
        User.GetComponent<PlayerController>().Weapon = this;
        Equiped = true;
        transform.parent = user.transform;
        transform.position = user.transform.position;
        GetComponent<Rigidbody2D>().isKinematic = true;
        GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        Rpc_Equip(user.GetComponent<NetworkIdentity>().netId);
    }
    [ClientRpc]
    public void Rpc_Equip(NetworkInstanceId userID)
    {
        PlayerController user = ClientScene.FindLocalObject(userID).GetComponent<PlayerController>();
        User = user.gameObject;
        User.GetComponent<PlayerController>().Weapon = this;
        Equiped = true;
        transform.parent = user.transform;
        transform.position = user.WeaponHoldPoint.position;
        GetComponent<Rigidbody2D>().isKinematic = true;
        GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        if (User.GetComponent<NetworkIdentity>().isLocalPlayer) GameManager.Instance.InfoCard.Populate(this);
    }
    public void Drop()
    {
        GetComponent<Rigidbody2D>().isKinematic = false;
        GetComponent<Rigidbody2D>().AddForce(Random.insideUnitCircle * 500);
        GetComponent<Rigidbody2D>().AddTorque(Random.Range(-400, 400));

        transform.parent = null;
        Equiped = false;
        User = null;
        Rpc_Drop();
    }
    [ClientRpc]
    public void Rpc_Drop()
    {
        GetComponent<Rigidbody2D>().isKinematic = false;
        GetComponent<Rigidbody2D>().AddForce(Random.insideUnitCircle * 500);
        GetComponent<Rigidbody2D>().AddTorque(Random.Range(-400 , 400));

        transform.parent = null;
        Equiped = false;
        User = null;
        GameManager.Instance.InfoCard.Clear();
    }
    [Server]
    private void UpdateLoyalties()
    {
        
        foreach(PlayerController c in FindObjectsOfType<PlayerController>())
        {
            if (Loyalties.ContainsKey(c))
            {
                if (User && User.GetComponent<PlayerController>() == c) Loyalties[c] = Loyalties[c] + LoyaltyGain > MaxLoyalty ? MaxLoyalty : Loyalties[c] + LoyaltyGain;
                else Loyalties[c] = Loyalties[c] - LoyaltyDecay < 0 ? 0 : Loyalties[c] - LoyaltyDecay;
            } else
            {
                Loyalties.Add(c, 0);
            }
            
        }
        foreach(var l in Loyalties)
        {
            Rpc_UpdateLoyalty(l.Key.GetComponent<NetworkIdentity>().netId, l.Value);
        }
    }

    [ClientRpc]
    void Rpc_UpdateLoyalty(NetworkInstanceId id , float loyalty)
    {
        if (!ClientScene.FindLocalObject(id)) return;
        var player = ClientScene.FindLocalObject(id).GetComponent<PlayerController>();
        if (player == null)
        {
            print("Unknown player fam");
            return;
        }
        if (Loyalties == null)
        {
            return;
        }
        if (Loyalties.ContainsKey(player))
        {
            Loyalties[player] = loyalty;
        } else
        {
            Loyalties.Add(player, loyalty);
        }

        
    }
    [ServerCallback]
    protected virtual void FixedUpdate()
    {
        UpdateLoyalties();
    }

    public void OnImmortal(bool value)
    {
        Immortal = value;
        if (Immortal)
        {
            FlameEffect.Play();
        }
    }
    public float GetLoyalty( PlayerController player)
    {
        if (Loyalties.ContainsKey(player)) return Loyalties[player] / MaxLoyalty;
        else return 0;
    }

    public void OnPlayerKilled( KillInfo info )
    {
        for (int i=0; i < ActiveChallenges.Count; i++)
        {
            ActiveChallenges[i].CheckCompletion(info);
        }
        if(info.LastHit.Weapon == this)
        {
            Malice = Malice + MaliceForKill > MaxMalice ? MaxMalice : Malice + MaliceForKill;
            if(Malice == MaxMalice)
            {
                GameManager.Instance.Rpc_Say(name + " has become an Immortal Instrument!");
                Immortal = true;
            }
        }
    }
    public void AddModifier(Modifier.Mods reward)
    {
        Modifiers.Add(reward);
    }
    public bool HasMod(Modifier.Mods mod)
    {
        return Modifiers.Contains(mod);
    }
    public float BuffSpeed ( float baseSpeed )
    {
        //TODO: Implement speed buffing
        return baseSpeed;
    }
    public float BuffSprintSpeed(float baseSprintSpeed)
    {
        //TODO: Implement speed buffing
        return baseSprintSpeed;
    }
    public float BuffMaxHealth (float baseHealth )
    {
        //TODO: Implement these health buffs
        return baseHealth;
    }
    public float BuffHealthRegenRate(float baseHealthRegenRate)
    {
        //TODO: Implement these health buffs
        return baseHealthRegenRate;
    }
    public float BuffHealthRegenDelay(float baseHealthRegenDelay)
    {
        //TODO: Implement these health buffs
        return baseHealthRegenDelay;
    }
    public float BuffMaxStamina(float baseStamina)
    {
        //TODO: Implement these Stamina buffs
        return baseStamina;
    }
    public float BuffStaminaRegenRate(float baseStaminaRegenRate)
    {
        //TODO: Implement these Stamina buffs
        return baseStaminaRegenRate;
    }
    public float BuffStaminaRegenDelay(float baseStaminaRegenDelay)
    {
        //TODO: Implement these Stamina buffs
        return baseStaminaRegenDelay;
    }
    public float ApplyDamageModifiers(float baseAmmount , PlayerController other)
    {
        //TODO: Implement this
        return baseAmmount;
    }
}
