﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class MeleeWeapon : BaseWeapon
{

    public Animator Anim;

    protected override void  Start()
    {
        base.Start();
        Anim = GetComponent<Animator>();
        if (!Network.isServer)
        {
            GetComponent<Collider2D>().enabled = false;
        }
    }
    public override void PrimaryAttack()
    {
        GetComponent<Collider2D>().enabled = true;
        Anim.Play("Swing1");     
    }
    public override void SecondaryAttack()
    {
        GetComponent<Collider2D>().enabled = true;
        Anim.Play("SpinSwing"); 
    }

    

    [ServerCallback]
    void OnTriggerEnter2D( Collider2D col )
    {
        var combat = col.GetComponent<PlayerController>();
        if(combat != null && combat != User.GetComponent<PlayerController>() && Equiped)
        {
            var id = User.GetComponent<NetworkIdentity>().netId;
            combat.Damage(TrueDamage , User.GetComponent<NetworkIdentity>().netId);
        }
    }
}
