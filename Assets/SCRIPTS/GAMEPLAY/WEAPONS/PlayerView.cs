﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class PlayerView : NetworkBehaviour {

    //A lot of these methods are going to be RPCs so that all clients will see them
    //However there are 2 versions, the Non RPC are to ensure that the action is performed on the server, these functions
    //Then call the client RPC to propagate the action to the views on the client

    public PlayerController MyController;
    public Slider RemoteHealthBar , RemoteStaminaBar;
    public Rigidbody2D Rigid;

    public void Move( Vector2 Direction )
    {
        Rigid.AddForce(Direction);
    } 

    [ClientRpc]
    void Rpc_Move( Vector2 Direction )
    {

    }

    public void PrimaryAttack( Vector2 Direction )
    {
        transform.up = Direction;
        MyController.Weapon.PrimaryAttack();
    }

    [ClientRpc]
    void Rpc_PrimaryAttack()
    {

    }

    public void SecondaryAttack( Vector2 Direction )
    {
        transform.up = Direction;
        MyController.Weapon.SecondaryAttack();
    }

    [ClientRpc]
    void Rpc_SecondaryAttack()
    {

    }


}
