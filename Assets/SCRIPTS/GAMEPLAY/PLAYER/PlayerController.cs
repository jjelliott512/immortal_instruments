﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class PlayerController : NetworkBehaviour {

    #region Movement and Combat Stats
    //Movement Stats | we are writing getters to let the weapons buff the speed of the player as they need
    public float BaseWalkSpeed, BaseSprintSpeed;
    public float WalkSpeed {  get { if (Weapon != null) { return Weapon.BuffSpeed(BaseWalkSpeed);} else { return BaseWalkSpeed; } } }
    public float SprintSpeed { get { if (Weapon != null) { return Weapon.BuffSprintSpeed(BaseSprintSpeed); } else { return BaseSprintSpeed; } } }

    //Health and stamina stats | again, deffering to the weapon to determine if anything needs to be buffed
    public float BaseMaxHealth, BaseHealthRegenRate, BaseHealthRegenDelay , BaseMaxStamina , BaseStaminaRegenRate , BaseStaminaRegenDelay;
    [SyncVar(hook = "DrawHealth")]
    public float Health;
    [SyncVar( hook = "DrawStamina")]
    public float Stamina;
    [SyncVar]
    public float PickupRange; //These dont need to be buffed ( I think )
    public float SprintCost;
    public float MaxHealth { get { if (Weapon != null) { return Weapon.BuffMaxHealth(BaseMaxHealth); } else { return BaseMaxHealth; } } }
    public float HealthRegenRate { get { if (Weapon != null) { return Weapon.BuffHealthRegenRate(BaseHealthRegenRate); } else { return BaseHealthRegenRate; } } }
    public float HealthRegenDelay { get { if (Weapon != null) { return Weapon.BuffHealthRegenDelay(BaseHealthRegenDelay); } else { return BaseHealthRegenDelay; } } }
    public float MaxStamina { get { if (Weapon != null) { return Weapon.BuffMaxStamina(BaseMaxStamina); } else { return BaseMaxStamina; } } }
    public float StaminaRegenRate { get { if (Weapon != null) { return Weapon.BuffStaminaRegenRate(BaseStaminaRegenRate); } else { return BaseStaminaRegenRate; } } }
    public float StaminaRegenDelay { get { if (Weapon != null) { return Weapon.BuffStaminaRegenDelay(BaseStaminaRegenDelay); } else { return BaseStaminaRegenDelay; } } }

    //Regen Stuff
    float LastDamageTime, LastConsumeTime;
    bool IsHealing = true, IsRegenerating = true;
    #endregion

    //References
    public BaseWeapon Weapon;
    public PlayerView MyView;
    public GameObject DamageNumberPrefab;
    KillInfo MyKillInfo;
    NetworkIdentity NetworkID;
    [SyncVar]
    bool IsAlive = false;
    public bool Ready = false;
    float RespawnTime = 5f;
    //UI references
    InfoCard WeaponDisplay;
    Slider HealthBar, StaminaBar;
    public Camera MyCamera;
    //Condition Stuff
    public List<Condition> Conditions;
    public List<Condition> ConditionsToRemove;
    public Transform WeaponHoldPoint;
    void Start()
    {
        if (isLocalPlayer)
        {
            StartCoroutine(Init());
        } else
        {
            NetworkID = GetComponent<NetworkIdentity>();
            MyKillInfo = new KillInfo(this);
            HealthBar = MyView.RemoteHealthBar;
            StaminaBar = MyView.RemoteStaminaBar;
            MyView.RemoteHealthBar.gameObject.SetActive(true);
            MyView.RemoteStaminaBar.gameObject.SetActive(true);
        }        
    }

    public IEnumerator Init()
    {
        while (!Ready) yield return new WaitForEndOfFrame();
        NetworkID = GetComponent<NetworkIdentity>();
        MyKillInfo = new KillInfo(this);
        if (NetworkID.hasAuthority)
        {
            HealthBar = GameManager.Instance.HealthBar;
            StaminaBar = GameManager.Instance.StaminaBar;
            WeaponDisplay = GameManager.Instance.InfoCard;
            Health = MaxHealth;
            Stamina = MaxStamina;
            IsAlive = true;
        }        
    }
    void Update()
    {
        DoRegen();
        if (!Ready || !IsAlive) return;
        if (!NetworkID.hasAuthority || !IsAlive)
        {
            return;
        }
        if (Input.GetButtonDown("Fire1") && Weapon && HasStamina(Weapon.PrimaryCost))
        {
            var dir = (Input.mousePosition - Camera.main.WorldToScreenPoint(transform.position)).normalized;
            transform.up = dir;
            Cmd_PrimaryAttack(dir);
        }
        if (Input.GetButtonDown("Fire2") && Weapon && HasStamina(Weapon.SecondaryCost))
        {
            var dir = (Input.mousePosition - Camera.main.WorldToScreenPoint(transform.position)).normalized;
            transform.up = dir;
            Cmd_SecondaryAttack(dir);
        }
        if (Input.GetButtonDown("Interact"))
        {
            Cmd_Equip();
        }
        if (Input.GetButtonDown("Drop"))
        {
            Cmd_Drop();
        }
        if (Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0)
        {
            
            Move((Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)) && HasStamina(SprintCost));
        }
    }

    void Move(bool sprinting)
    {
        if(sprinting)
        {
            MyView.Move(new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical")) * SprintSpeed);
            transform.up = GetComponent<Rigidbody2D>().velocity.normalized;
            Cmd_Consume(SprintCost);
        } else
        {
            transform.up = GetComponent<Rigidbody2D>().velocity.normalized;
            MyView.Move(new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical")) * WalkSpeed);
        }
    }
    [Command]
    void Cmd_PrimaryAttack( Vector2 direction )
    {
        Cmd_Consume(Weapon.PrimaryCost);
        MyView.PrimaryAttack(direction);
    }
    [Command]
    void Cmd_SecondaryAttack( Vector2 direction )
    {
        Cmd_Consume(Weapon.SecondaryCost);
        MyView.SecondaryAttack(direction);
    }
    [Command]
    void Cmd_Equip()
    {
        BaseWeapon closest = null;
        float distance = float.MaxValue;
        foreach (var weapon in FindObjectsOfType<BaseWeapon>()){
            if(!weapon.Equiped && Vector3.Distance(transform.position , weapon.transform.position) < distance)
            {
                closest = weapon;
                distance = Vector3.Distance(transform.position, weapon.transform.position);
            }
        }
        if( distance < PickupRange )
        {
            if (Weapon != null)
                Cmd_Drop();

            Weapon = closest;
            Weapon.Equip(this);
        }
    }
    [Command]
    void Cmd_Drop()
    {
        if(Weapon != null)
        {
            Weapon.Drop();
            Weapon = null;            
        }
    }

    public void Damage(float amount, NetworkInstanceId otherID)
    {
        var other = ClientScene.FindLocalObject(otherID).GetComponent<PlayerController>();
        MyKillInfo.AddDamage(other, amount);
        var num = Instantiate(DamageNumberPrefab, (Vector2)transform.position + Random.insideUnitCircle, Quaternion.identity);
        NetworkServer.Spawn(num);
        num.GetComponent<DamageNumber>().Rpc_Init(amount);
        IsHealing = false;
        LastDamageTime = Time.time;
        Health -= amount;
        if (Health < 0) Die(other);

    }
    public bool HasStamina(float amount)
    {
        return Stamina > amount;
    }
    [Command]
    public void Cmd_Consume(float amount)
    {
        Stamina -= amount;
        IsRegenerating = false;
        LastConsumeTime = Time.time;
    }


    [ServerCallback]
    void DoRegen()
    {
        if (Time.time - LastDamageTime > HealthRegenDelay)
        {
            IsHealing = true;
            if (MyKillInfo == null) MyKillInfo = new KillInfo(this);
            MyKillInfo.Reset();
        }
        if (Time.time - LastConsumeTime > StaminaRegenDelay) IsRegenerating = true;
        if (IsHealing) Health = Health + HealthRegenRate > MaxHealth ? MaxHealth : Health + HealthRegenRate;
        if (IsRegenerating) Stamina = Stamina + StaminaRegenRate > MaxStamina ? MaxStamina : Stamina + StaminaRegenRate;
    }
    void DrawUI()
    {
        if (!IsAlive) return;
        StaminaBar.maxValue = MaxStamina;
        StaminaBar.value = Stamina;
        HealthBar.maxValue = MaxHealth;
        HealthBar.value = Health;
    }

    void DrawHealth(float value)
    {
        if(Health != value)
        print("I took some damage");
        Health = value;
        DrawUI();
    }
    void DrawStamina(float value)
    {
        Stamina = value;
        DrawUI();
    }

    [Server]
    void Die (PlayerController killer)
    {
        MyKillInfo.LastHit = killer;
        GameManager.Instance.OnPlayerKilled(MyKillInfo);
        IsAlive = false;
        Rpc_OnDeath();
        if(killer.Weapon.Immortal == false)
        {
            print("Non-immortal, respawning");
            StartCoroutine(Respawn());
        }
    }

    [ClientRpc]
    void Rpc_OnDeath()
    {
        if (!isLocalPlayer) return;
        MyCamera.gameObject.SetActive(false);
        GameManager.Instance.SceneCamera.SetActive(true);
        transform.position = new Vector3(-100, -100 - 100);
    }
    IEnumerator Respawn()
    {
        yield return new WaitForSeconds(RespawnTime);
        IsAlive = true;
        Health = MaxHealth;
        Stamina = MaxStamina;
        Rpc_OnRespawn();
        //TODO: better this
    }
    [ClientRpc]
    void Rpc_OnRespawn()
    {
        if (!isLocalPlayer) return;
        MyCamera.gameObject.SetActive(true);
        GameManager.Instance.SceneCamera.SetActive(false);
        transform.position = GameManager.GetSpawn(Random.Range(0, 10));
        MyCamera.transform.position = transform.position + new Vector3(0,0, -10);
    }
}
