﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoloKillsChallenge : Challenge
{
    public override void CheckCompletion(KillInfo Info)
    {
        if (Info.DamageByPlayer.Count == 1 && Owner.Equiped && Info.DamageByPlayer.ContainsKey(Owner.User.GetComponent<PlayerController>()))
        {
            CurrentCount++;
            if (CurrentCount == NeededCount) OnCompletion();
        }
    }
}
