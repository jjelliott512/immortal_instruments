﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstHitChallenge : Challenge
{
    public override void CheckCompletion(KillInfo Info)
    {
        if (Owner.User && Info.FirstHit == Owner.User.GetComponent<PlayerController>() && Owner.Equiped) CurrentCount++;
        if (CurrentCount == NeededCount) OnCompletion();
    }

}
