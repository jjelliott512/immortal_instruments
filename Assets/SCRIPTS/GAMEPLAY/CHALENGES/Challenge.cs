﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
public abstract class Challenge : NetworkBehaviour {

    public BaseWeapon Owner;
    protected Challenge[] NextChallenges;
    public Modifier.Mods Reward;
    [SyncVar(hook = "OnChange")]
    public int CurrentCount = 0;
    public int NeededCount;
    public string RewardName, ChallengeInfo;
    public abstract void CheckCompletion( KillInfo Info);
    public virtual void OnCompletion()
    {
        GameManager.Instance.Rpc_Say(Owner.gameObject.name + " is now " + RewardName + "!");
        Owner.AddModifier(Reward);
        Owner.ActiveChallenges.Remove(this);
    }
    protected void OnChange(int val)
    {
        print("challenge update");
        CurrentCount = val;
    }

}
