﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class ImmortalNetworkManager : NetworkLobbyManager
{
    public float SpawnRadius;
    public void HostGame()
    {
        networkAddress = "localhost";
        networkPort = 7777;
        StartHost();
    }

    public void JoinGame()
    {
        networkAddress = "localhost";
        networkPort = 7777;
        StartClient();
    }

    public override void OnLobbyStartHost()
    {
        base.OnLobbyStartHost();
    }

    public override GameObject OnLobbyServerCreateLobbyPlayer(NetworkConnection conn, short playerControllerId)
    {
        var card = Instantiate(lobbyPlayerPrefab).gameObject;
        card.GetComponent<PlayerCard>().Init("Default Player", this, conn);
        return card;
    }

    public override void OnLobbyServerSceneChanged(string sceneName)
    {
        if(sceneName == playScene)
        {
            var gm = FindObjectOfType<GameManager>();
            gm.LivingPlayers = new List<PlayerController>();
            int index = 0;
            foreach(var c in NetworkServer.connections)
            {
                Instantiate(gamePlayerPrefab, Quaternion.AngleAxis((360 / NetworkServer.connections.Count), Vector3.forward) * Vector2.up * SpawnRadius, Quaternion.identity);
                NetworkServer.AddPlayerForConnection(c, gamePlayerPrefab, (short)index);
                gm.LivingPlayers.Add(gamePlayerPrefab.GetComponent<PlayerController>());
                index++;
            }
            
            gm.SetPlayerCount(NetworkServer.connections.Count);
        }
    }
}
