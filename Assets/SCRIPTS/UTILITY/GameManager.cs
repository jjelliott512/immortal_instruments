﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System;

public class GameManager : NetworkBehaviour {

    List<BaseWeapon> Weapons;
    [SerializeField]
    public List<GameObject> WeaponPrefabs;
    public Text Killfeed;
    public InfoCard InfoCard;
    public static GameManager Instance { get { return FindObjectOfType<GameManager>(); } }
    public Slider HealthBar, StaminaBar;
    public static PlayerController LocalPlayer;
    public GameObject PlayerPrefab , SceneCamera ;
    [SerializeField]
    public static float SpawnRadius = 20;
    [SyncVar]
    public int NumPlayers = 0;
    [SyncVar]
    public int PlayersAlive = 0;
    public List<PlayerController> LivingPlayers;
    public EndGamePanel EndGamePanel;
    public float SkySpeed;
    float CurrentSkyRot = 0;
    public void Start()
    {
        Weapons = new List<BaseWeapon>();
        foreach (var w in FindObjectsOfType<BaseWeapon>()) Weapons.Add(w);

        foreach (var p in FindObjectsOfType<PlayerController>()){
            if (p.isLocalPlayer) LocalPlayer = p;
        }
        LocalPlayer.Ready = true;
        LocalPlayer.transform.position = GetSpawn(LocalPlayer.playerControllerId);

        SpawnWeapons();

        
    }


    [Server]
    void SpawnWeapons()
    {
            NumPlayers = NetworkServer.connections.Count;
            for (int i = 0; i < NumPlayers; i++)
            {
                var weap = Instantiate(WeaponPrefabs[UnityEngine.Random.Range(0, WeaponPrefabs.Count)], 2 * GetSpawn(i) / 3, Quaternion.identity);
                NetworkServer.Spawn(weap);
            }
        Rpc_StartGame();
    }

    internal void SetPlayerCount(int count)
    {
        NumPlayers = PlayersAlive = count;
    }

    [ClientRpc]
    void Rpc_StartGame()
    {
        foreach(var w in FindObjectsOfType<BaseWeapon>()){
            Weapons.Add(w);
        }
    }
    public static Vector3 GetSpawn(int index)
    {
        return Quaternion.AngleAxis(40 * index, Vector3.forward) * Vector2.up * SpawnRadius;
         
    }
    public void OnPlayerKilled(KillInfo info)
    {
        foreach (var w in Weapons) w.OnPlayerKilled(info);
        if (info.LastHit.Weapon.Immortal) LivingPlayers.Remove(info.Owner);
        if (LivingPlayers.Count == 1) EndGame();
        else PrintKillInfo(info);
    } 
    [ClientRpc]
    public void Rpc_Say(string message)
    {
        Killfeed.text = message + "\n" + Killfeed.text;
    }

    public void PrintKillInfo(KillInfo info) {
        Rpc_Say(info.Owner.gameObject.name + " was killed by " + info.LastHit.gameObject.name +"'s " + info.LastHit.Weapon.gameObject.name);
    }

    void EndGame()
    {
        EndGamePanel.Show(LivingPlayers[0]);
    }

    void Update()
    {
        CurrentSkyRot += SkySpeed;
        RenderSettings.skybox.SetFloat("_Rotation", CurrentSkyRot);
    }
}
