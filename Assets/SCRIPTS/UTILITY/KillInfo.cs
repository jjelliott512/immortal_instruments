﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillInfo {

    public Dictionary<PlayerController, float> DamageByPlayer;
    public PlayerController FirstHit, LastHit , Owner;

    public KillInfo(PlayerController owner)
    {
        Owner = owner;
        DamageByPlayer = new Dictionary<PlayerController, float>();
        LastHit = null;
    }


    public void AddDamage(PlayerController other , float amt)
    {
        if (DamageByPlayer.Count == 0) FirstHit = other;
        if (DamageByPlayer.ContainsKey(other))
        {
            DamageByPlayer[other] += amt;
        } else
        {
            DamageByPlayer.Add(other, amt);
        }
    }

    public void Reset()
    {
        DamageByPlayer.Clear();
        FirstHit = LastHit = null;
    } 


}
